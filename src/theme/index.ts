import { extendTheme } from "@chakra-ui/react";

const fonts = {
  oldschool: "'Oldschool Grotesk', sans-serif",
  recoleta: "'Recoleta', sans-serif",
}

const theme = extendTheme({ fonts });

export default theme;