import { Box, Text } from "@chakra-ui/react";
import CustomCarousel from "../components/CarouselWrapper";

const slides = [
  {
    image: "/images/blog2.png",
    tags: ["Management"],
    title: "Which is better working from office or working from home?",
  },
  {
    image: "/images/blog1.png",
    tags: ["Design", "Development"],
    title: "Why TIF always do a prior brainstorming.",
  },
  {
    image: "/images/blog1.png",
    tags: ["Design", "UX UI Design"],
    title: "Blogging for Business: How to Use Your Blog to Boost Your Brand",
  },
];

const Carousel: React.FC = () => {
  return (
    <Box bg="black" color="white" p="53px 151px">
      <Text
        fontFamily="recoleta"
        fontSize="60px"
        lineHeight="66px"
        fontWeight={500}
        textAlign="center"
        mb="80px"
      >
        You might also like
      </Text>
      <CustomCarousel data={slides} />
    </Box>
  );
};

export default Carousel;
