const getStyles = (disabled: boolean) => {
    return {
      bg: disabled ? "#FFF6" : "white",
      cursor: disabled ? "not-allowed" : "pointer",
      _hover: { bg: disabled ? "#FFF6" : "#FFFA" },
      rounded: "full",
      p: "13px",
    };
};
  
export default getStyles;