export default interface CarouselSlideProps {
    image: string
    tags: string[]
    title: string;
}