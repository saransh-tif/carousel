export type CustomButtonGroupProps = {
  next?: () => void;
  previous?: () => void;
  currentSlide?: number;
  totalSlides: number;
  isMobile: boolean;
};
