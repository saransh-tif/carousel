import CarouselSlideProps from "@/types/CarouselSlideProps";
import { Box, Flex, IconButton, Image, useMediaQuery } from "@chakra-ui/react";
import Carousel, { ResponsiveType } from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import CarouselSlide from "./CarouselSlide";
import { CustomButtonGroup } from "./CustomButtonGroup";

type CustomCarouselProps = {
  data: CarouselSlideProps[];
};

const responsive: ResponsiveType = {
  desktop: {
    breakpoint: { max: 3000, min: 1440 },
    items: 2,
    partialVisibilityGutter: 30,
  },
  tablet: {
    breakpoint: { max: 1440, min: 464 },
    items: 1,
    partialVisibilityGutter: 0,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    partialVisibilityGutter: 0,
  },
};

const CustomCarousel: React.FC<CustomCarouselProps> = ({ data }) => {
  const totalSlides = data.length;
  const [isMobile] = useMediaQuery("(max-width: 1440px)");

  return (
    <Box py={4}>
      <Carousel
        responsive={responsive}
        renderButtonGroupOutside
        partialVisible={!isMobile}
        arrows={false}
        infinite={false}
        customButtonGroup={<CustomButtonGroup totalSlides={totalSlides} isMobile={isMobile} />}
        itemClass="carousel-item"
        containerClass="carousel-container"
      >
        {data.map((item, index) => (
          <CarouselSlide key={index} {...item} />
        ))}
      </Carousel>
    </Box>
  );
};

export default CustomCarousel;
