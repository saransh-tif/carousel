import { CustomButtonGroupProps } from "@/types/CustomButtonGroupProps";
import getStyles from "@/utils/getStyles";
import { Flex, IconButton, Image } from "@chakra-ui/react";

export const CustomButtonGroup: React.FC<CustomButtonGroupProps> = ({
    next,
    previous,
    currentSlide,
    totalSlides,
    isMobile,
  }) => {
    const prevDisabled = currentSlide === 0;
    const nextDisabled = currentSlide === totalSlides - (isMobile ? 1 : 2);

    return (
      <Flex alignItems="center" justifyContent="center" gap={4}>
        <IconButton
          aria-label="previous"
          icon={<Image src="/icons/left.svg" alt="left" />}
          onClick={previous}
          {...getStyles(prevDisabled)}
        />
        <IconButton
          aria-label="next"
          icon={<Image src="/icons/right.svg" alt="right" />}
          onClick={next}
          {...getStyles(nextDisabled)}
        />
      </Flex>
    );
  };