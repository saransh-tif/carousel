import CarouselSlideProps from "@/types/CarouselSlideProps";
import { Box, Flex, Image, Text } from "@chakra-ui/react";

const CarouselSlide: React.FC<CarouselSlideProps> = ({
  image,
  tags,
  title,
}) => {
  return (
    <Box mx="22px">
      <Image src={image} alt="blog" />
      <Box
        transform="translateY(-50px)"
        p="30px"
        bg="#1E1E1E"
        borderRadius="20px"
        w="89%"
        mx="auto"
        h="100%"
      >
        <Flex gap={4} mb="10px" flexWrap="wrap">
          {tags.map((tag, idx) => (
            <Text
              fontFamily="oldschool"
              fontSize="12px"
              lineHeight="18px"
              fontWeight={400}
              key={idx}
              borderRadius="100px"
              p="8px 18px"
              border="1px solid #FFFFFF"
            >
              {tag}
            </Text>
          ))}
        </Flex>

        <Text
          fontFamily="recoleta"
          fontSize="30px"
          lineHeight="36px"
          fontWeight={500}
          mb="20px"
        >
          {title}
        </Text>

        <Flex align="center" gap="8px">
          <Image src="/images/avatar.png" alt="avatar" />
          <Box fontFamily="oldschool" fontWeight={400}>
            <Text fontSize="10px" lineHeight="18px">
              Rudra Ghosh
            </Text>
            <Text fontSize="8px" lineHeight="12px">
              Product Desinger at TIF
            </Text>
          </Box>
        </Flex>
      </Box>
    </Box>
  );
};

export default CarouselSlide;
